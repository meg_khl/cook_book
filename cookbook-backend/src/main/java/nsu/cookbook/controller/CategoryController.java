package nsu.cookbook.controller;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.CategoryDto;
import nsu.cookbook.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/get-all")
    public List<CategoryDto> getAllCategories() {
        return categoryService.getAllCategories();
    }
}
