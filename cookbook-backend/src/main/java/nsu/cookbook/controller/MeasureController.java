package nsu.cookbook.controller;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.service.MeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/measure")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
public class MeasureController {
    private final MeasureService measureService;

    @GetMapping("get-all")
    public List<String> getMeasures() {
        return measureService.getMeasures();
    }
}
