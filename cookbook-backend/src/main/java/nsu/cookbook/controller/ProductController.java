package nsu.cookbook.controller;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
public class ProductController {
    private final ProductService productService;

    @GetMapping("/get-all")
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @PostMapping("/add")
    public ProductDto addProduct(@RequestBody ProductDto productDto) {
        return productService.addProduct(productDto);
    }

    @GetMapping("/get-by-name-part")
    public List<ProductDto> getProductsByNamePart(String namePart) {
        return productService.getProductsByNamePart(namePart);
    }
}
