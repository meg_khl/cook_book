package nsu.cookbook.controller;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.dto.RecipeDto;
import nsu.cookbook.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recipes")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
public class RecipeController {
    private final RecipeService recipeService;

    @GetMapping("/get-all")
    public List<BaseRecipeDto> getAll() {
        return recipeService.getAll();
    }

    @PostMapping("/add-recipe")
    public RecipeDto addRecipe(@RequestBody RecipeDto recipeDto) {
        return recipeService.addRecipe(recipeDto);
    }

    @GetMapping("get-base-by-categories-and-products")
    public List<BaseRecipeDto> getBaseRecipesByCategoriesAndProducts(@RequestParam(required = false) List<Integer> categoryIds,
                                                                     @RequestParam(required = false) List<Integer> productIds) {
        return recipeService.getBaseRecipesByCategoriesAndProducts(categoryIds,
                productIds);
    }

    @GetMapping("/get-by-id")
    public RecipeDto getRecipeById(@RequestParam("id") int id) {
        return recipeService.getRecipeById(id);
    }
}
