package nsu.cookbook.converter;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BaseRecipeConverter {
    private final CategoryConverter categoryConverter;

    public BaseRecipeDto convertRecipe(Recipe recipe) {
        BaseRecipeDto baseRecipeDto = new BaseRecipeDto();
        baseRecipeDto.setId(recipe.getId());
        baseRecipeDto.setName(recipe.getName());

        baseRecipeDto.setCategories(recipe.getCategories()
                .stream()
                .map(categoryConverter::convertCategory)
                .collect(toList()));

        return baseRecipeDto;
    }

    public List<BaseRecipeDto> getBaseRecipeDtos(List<Recipe> recipes) {
        if (recipes == null) {
            return emptyList();
        }
        return recipes.stream().map(this::convertRecipe).collect(toList());
    }
}
