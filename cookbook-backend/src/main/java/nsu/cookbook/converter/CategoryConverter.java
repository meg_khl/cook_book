package nsu.cookbook.converter;

import nsu.cookbook.dto.CategoryDto;
import nsu.cookbook.model.Category;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
public class CategoryConverter {
    public CategoryDto convertCategory(Category category) {
        CategoryDto dto = new CategoryDto();
        dto.setId(category.getId());
        dto.setName(category.getName());
        return dto;
    }

    public Category convertCategoryDto(CategoryDto categoryDto) {
        return new Category(categoryDto.getId(),
                categoryDto.getName());
    }

    public List<CategoryDto> getCategoryDtos(List<Category> categories) {
        if (categories == null) {
            return emptyList();
        }
        return categories.stream()
                .map(this::convertCategory)
                .collect(toList());
    }

    List<Category> getCategories(List<CategoryDto> categoryDtos) {
        if (categoryDtos == null) {
            return emptyList();
        }
        return categoryDtos.stream()
                .map(this::convertCategoryDto)
                .collect(toList());
    }
}
