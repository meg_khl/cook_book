package nsu.cookbook.converter;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.IngredientDto;
import nsu.cookbook.model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class IngredientConverter {
    private final ProductConverter productConverter;
    private final MeasureConverter measureConverter;

    public Ingredient getIngredient(IngredientDto ingredientDto) {
        return new Ingredient(ingredientDto.getId(),
                ingredientDto.getAmount(),
                productConverter.getProduct(ingredientDto.getProductDto()),
                measureConverter.getMeasure(ingredientDto.getMeasure()));
    }

    public IngredientDto getIngredientDto(Ingredient ingredient) {
        return new IngredientDto(ingredient.getId(),
                ingredient.getAmount(),
                measureConverter.getMeasureDto(ingredient.getMeasure()),
                productConverter.getProductDto(ingredient.getProduct()));
    }

    List<Ingredient> getIngredients(List<IngredientDto> ingredientDtos) {
        if (ingredientDtos == null) {
            return emptyList();
        }
        return ingredientDtos.stream()
                .map(this::getIngredient)
                .collect(toList());
    }

    List<IngredientDto> getIngredientDtos(List<Ingredient> ingredients) {
        if (ingredients == null) {
            return emptyList();
        }
        return ingredients.stream()
                .map(this::getIngredientDto)
                .collect(toList());
    }
}
