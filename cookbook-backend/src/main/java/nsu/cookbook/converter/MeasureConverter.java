package nsu.cookbook.converter;

import nsu.cookbook.model.Measure;
import org.springframework.stereotype.Component;

@Component
public class MeasureConverter {
    public Measure getMeasure(String measure) {
        return new Measure(measure);
    }

    public String getMeasureDto(Measure measure) {
        return measure.getName();
    }
}
