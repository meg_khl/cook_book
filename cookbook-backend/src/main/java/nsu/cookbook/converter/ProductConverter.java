package nsu.cookbook.converter;

import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
public class ProductConverter {
    public Product getProduct(ProductDto productDto) {
        return new Product(productDto.getId(), productDto.getName());
    }

    public ProductDto getProductDto(Product product) {
        return new ProductDto(product.getId(), product.getName());
    }

    public List<ProductDto> getProductDtos(List<Product> products) {
        if (products == null) {
            return emptyList();
        }
        return products.stream()
                .map(this::getProductDto)
                .collect(toList());
    }
}
