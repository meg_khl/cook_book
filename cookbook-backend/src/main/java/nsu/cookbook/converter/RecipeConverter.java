package nsu.cookbook.converter;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dto.RecipeDto;
import nsu.cookbook.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RecipeConverter {
    private final IngredientConverter ingredientConverter;
    private final CategoryConverter categoryConverter;
    private final StepConverter stepConverter;

    public Recipe getRecipe(RecipeDto recipeDto) {
        return new Recipe(recipeDto.getId(),
                recipeDto.getName(),
                ingredientConverter.getIngredients(recipeDto.getIngredients()),
                categoryConverter.getCategories(recipeDto.getCategories()),
                stepConverter.getSteps(recipeDto.getSteps()));
    }

    public RecipeDto getRecipeDto(Recipe recipe) {
        return new RecipeDto(recipe.getId(),
                recipe.getName(),
                categoryConverter.getCategoryDtos(recipe.getCategories()),
                ingredientConverter.getIngredientDtos(recipe.getIngredients()),
                stepConverter.getStepDtos(recipe.getSteps())
        );
    }
}
