package nsu.cookbook.converter;

import nsu.cookbook.dto.StepDto;
import nsu.cookbook.model.Step;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
public class StepConverter {
    public Step getStep(StepDto stepDto) {
        return new Step(stepDto.getId(),
                stepDto.getDescription(),
                stepDto.getStepNumber());
    }

    public StepDto getStepDto(Step step) {
        return new StepDto(step.getId(),
                step.getStepNumber(),
                step.getDescription());
    }

    List<StepDto> getStepDtos(List<Step> steps) {
        if (steps == null) {
            return emptyList();
        }
        return steps.stream()
                .map(this::getStepDto)
                .collect(toList());
    }

    List<Step> getSteps(List<StepDto> stepDtos) {
        if (stepDtos == null) {
            return emptyList();
        }
        return stepDtos.stream()
                .map(this::getStep)
                .collect(toList());
    }
}
