package nsu.cookbook.dao;

import nsu.cookbook.model.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryDao extends CrudRepository<Category, Integer> {
    List<Category> findAll();
}
