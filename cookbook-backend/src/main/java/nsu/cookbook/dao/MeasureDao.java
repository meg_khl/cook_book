package nsu.cookbook.dao;

import nsu.cookbook.model.Measure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeasureDao extends CrudRepository<Measure, String> {
    @Override
    List<Measure> findAll();
}
