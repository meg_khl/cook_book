package nsu.cookbook.dao;

import nsu.cookbook.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductDao extends CrudRepository<Product, Integer> {
    @Override
    List<Product> findAll();

    Optional<Product> findByName(String name);

    @Query(value = "select * from product where name ILIKE concat('%', :namePart, '%')",
            nativeQuery = true)
    List<Product> findByNamePart(@Param("namePart") String namePart);
}
