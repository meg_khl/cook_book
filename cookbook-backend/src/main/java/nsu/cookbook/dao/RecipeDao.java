package nsu.cookbook.dao;

import nsu.cookbook.model.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeDao extends CrudRepository<Recipe, Integer> {
    @Override
    List<Recipe> findAll();

    @Query(value = "SELECT * from recipe where id IN (" +
            "(SELECT recipe_id from recipe_category where category_id IN (:categoryIds))" +
            " INTERSECT " +
            "SELECT recipe_id from ingredient where product_id IN (:productIds))",
            nativeQuery = true)
    List<Recipe> findByCategoryIdsAndProductIds(@Param("categoryIds") List<Integer> categoryIds,
                                                @Param("productIds") List<Integer> productIds);

    @Query(value = "SELECT * from recipe where id IN (" +
            "(SELECT recipe_id from recipe_category where category_id IN (:categoryIds)))",
            nativeQuery = true)
    List<Recipe> findByCategoryIds(@Param("categoryIds") List<Integer> categoryIds);

    @Query(value = "SELECT * from recipe where id IN (" +
            "(SELECT recipe_id from ingredient where product_id IN (:productIds)))",
            nativeQuery = true)
    List<Recipe> findByProductIds(@Param("productIds") List<Integer> productIds);
}
