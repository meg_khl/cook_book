package nsu.cookbook.dao;

import nsu.cookbook.model.Step;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StepDao extends CrudRepository<Step, Integer> {
}
