package nsu.cookbook.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class RecipeDto extends BaseRecipeDto {
    private List<IngredientDto> ingredients;
    private List<StepDto> steps;

    public RecipeDto(int id,
                     String name,
                     List<CategoryDto> categories,
                     List<IngredientDto> ingredients,
                     List<StepDto> steps) {
        super(id, name, categories);
        this.ingredients = ingredients;
        this.steps = steps;
    }
}
