package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.converter.CategoryConverter;
import nsu.cookbook.dao.CategoryDao;
import nsu.cookbook.dto.CategoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryService {
    private final CategoryDao categoryDao;
    private final CategoryConverter categoryConverter;

    public List<CategoryDto> getAllCategories() {
        return categoryConverter.getCategoryDtos(categoryDao.findAll());
    }
}
