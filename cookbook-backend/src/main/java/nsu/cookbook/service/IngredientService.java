package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dao.IngredientDao;
import nsu.cookbook.model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class IngredientService {
    private final IngredientDao ingredientDao;
    private final ProductService productService;
    private final MeasureService measureService;

    public void addIngredients(List<Ingredient> ingredients) {
        if (ingredients == null) {
            return;
        }
        for (Ingredient ingredient : ingredients) {
            productService.addProduct(ingredient.getProduct());
            measureService.addMeasure(ingredient.getMeasure());
        }
        ingredientDao.saveAll(ingredients);
    }
}
