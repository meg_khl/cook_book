package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dao.MeasureDao;
import nsu.cookbook.model.Measure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MeasureService {
    private final MeasureDao measureDao;

    public List<String> getMeasures() {
        List<String> measureNames;
        List<Measure> measures = measureDao.findAll();
        if (measures == null) {
            return emptyList();
        }
        measureNames = measures.stream()
                .map(Measure::getName).collect(Collectors.toList());
        return measureNames;
    }

    public void addMeasure(Measure measure) {
        measureDao.save(measure);
    }
}
