package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.converter.ProductConverter;
import nsu.cookbook.dao.ProductDao;
import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductService {
    private final ProductDao productDao;
    private final ProductConverter productConverter;

    public void addProduct(Product product) {
        productDao.save(product);
    }

    public List<ProductDto> getAllProducts() {
        return productConverter.getProductDtos(productDao.findAll());
    }

    public ProductDto addProduct(ProductDto productDto) {
        return productDao.findByName(productDto.getName())
                .map(product -> productDto)
                .orElseGet(() -> productConverter
                        .getProductDto(productDao
                                .save(productConverter.getProduct(productDto))));
    }

    public List<ProductDto> getProductsByNamePart(String namePart) {
        return productConverter
                .getProductDtos(productDao.findByNamePart(namePart));
    }
}
