package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.converter.BaseRecipeConverter;
import nsu.cookbook.converter.RecipeConverter;
import nsu.cookbook.dao.RecipeDao;
import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.dto.RecipeDto;
import nsu.cookbook.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RecipeService {
    private final RecipeDao recipeDao;
    private final BaseRecipeConverter baseRecipeConverter;
    private final RecipeConverter recipeConverter;

    private final StepService stepService;
    private final IngredientService ingredientService;

    public List<BaseRecipeDto> getAll() {
        List<Recipe> recipes = recipeDao.findAll();
        return baseRecipeConverter.getBaseRecipeDtos(recipes);
    }

    public RecipeDto addRecipe(RecipeDto recipeDto) {
        Recipe convertedRecipe = recipeConverter.getRecipe(recipeDto);
        stepService.addSteps(convertedRecipe.getSteps());
        ingredientService.addIngredients(convertedRecipe.getIngredients());
        return recipeConverter.getRecipeDto(recipeDao.save(convertedRecipe));
    }

    public RecipeDto getRecipeById(int id) {
        Optional<Recipe> foundRecipe = recipeDao.findById(id);
        return foundRecipe.map(recipeConverter::getRecipeDto).orElse(null);
    }

    public List<BaseRecipeDto> getBaseRecipesByCategoriesAndProducts(List<Integer> categoryIds,
                                                                     List<Integer> productIds) {
        if (categoryIds == null || categoryIds.isEmpty()) {
            return getBaseByProducts(productIds);
        }
        if (productIds == null || productIds.isEmpty()) {
            return getBaseRecipesByCategories(categoryIds);
        }
        return baseRecipeConverter
                .getBaseRecipeDtos(recipeDao
                        .findByCategoryIdsAndProductIds(categoryIds, productIds));
    }

    private List<BaseRecipeDto> getBaseRecipesByCategories(List<Integer> categoryIds) {
        return baseRecipeConverter
                .getBaseRecipeDtos(recipeDao
                        .findByCategoryIds(categoryIds));
    }

    private List<BaseRecipeDto> getBaseByProducts(List<Integer> productIds) {
        return baseRecipeConverter
                .getBaseRecipeDtos(recipeDao
                        .findByProductIds(productIds));
    }
}
