package nsu.cookbook.service;

import lombok.RequiredArgsConstructor;
import nsu.cookbook.dao.StepDao;
import nsu.cookbook.model.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StepService {
    private final StepDao stepDao;

    public void addSteps(List<Step> steps) {
        if (steps == null) {
            return;
        }
        stepDao.saveAll(steps);
    }
}
