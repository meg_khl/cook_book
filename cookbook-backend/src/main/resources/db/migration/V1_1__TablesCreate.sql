create table measure
(
    measure text not null
        constraint measure_pk
            primary key
);

alter table measure
    owner to postgres;

create table if not exists product
(
    id   serial not null
        constraint product_pk
            primary key,
    name text   not null
);

alter table product
    owner to postgres;

create table if not exists recipe
(
    id   serial not null
        constraint recipe_pk
            primary key,
    name text   not null
);

alter table recipe
    owner to postgres;

create table if not exists category
(
    id   serial not null,
    name text   not null
);

alter table category
    owner to postgres;

create unique index if not exists category_id_uindex
    on category (id);

create table if not exists recipe_category
(
    recipe_id   integer
        constraint recipe_category_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    category_id integer
        constraint recipe_category_category_id_fk
            references category (id)
            on update cascade on delete cascade
);

alter table recipe_category
    owner to postgres;

create table if not exists step
(
    recipe_id   integer
        constraint step_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    description text   not null,
    step_number integer,
    id          serial not null
        constraint step_pk
            primary key
);

alter table step
    owner to postgres;

create table if not exists ingredient
(
    product_id integer
        constraint ingredient_product_id_fk
            references product
            on update cascade on delete cascade,
    amount     integer,
    recipe_id  integer
        constraint ingredient_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    measure    text
        constraint ingredient_measure_measure_fk
            references measure
            on update cascade on delete cascade,
    id         serial not null
        constraint ingredient_pk
            primary key
);

alter table ingredient
    owner to postgres;

CREATE SEQUENCE hibernate_sequence START 1;