create table if not exists measure
(
    measure text not null
        constraint measure_pk
            primary key
);

alter table measure
    owner to postgres;

create table if not exists product
(
    id   serial not null
        constraint product_pk
            primary key,
    name text   not null
);

alter table product
    owner to postgres;

create table if not exists recipe
(
    id   serial not null
        constraint recipe_pk
            primary key,
    name text   not null
);

alter table recipe
    owner to postgres;

create table if not exists category
(
    id   serial not null,
    name text   not null
);

alter table category
    owner to postgres;

create unique index if not exists category_id_uindex
    on category (id);

create table if not exists recipe_category
(
    recipe_id   integer
        constraint recipe_category_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    category_id integer
        constraint recipe_category_category_id_fk
            references category (id)
            on update cascade on delete cascade
);

alter table recipe_category
    owner to postgres;

create table if not exists step
(
    recipe_id   integer
        constraint step_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    description text   not null,
    step_number integer,
    id          serial not null
        constraint step_pk
            primary key
);

alter table step
    owner to postgres;

create table if not exists ingredient
(
    product_id integer
        constraint ingredient_product_id_fk
            references product
            on update cascade on delete cascade,
    amount     integer,
    recipe_id  integer
        constraint ingredient_recipe_id_fk
            references recipe
            on update cascade on delete cascade,
    measure    text
        constraint ingredient_measure_measure_fk
            references measure
            on update cascade on delete cascade,
    id         serial not null
        constraint ingredient_pk
            primary key
);


alter table ingredient
    owner to postgres;

insert into measure
values ('стакан 200 мл'),
       ('столовая ложка'),
       ('десертная ложка'),
       ('чайная ложка'),
       ('шт'),
       ('г'),
       ('кг'),
       ('литр'),
       ('грамм'),
       ('банка'),
       ('головка'),
       ('зубчик'),
       ('кусок'),
       ('миллилитр'),
       ('на кончике ножа'),
       ('по вкусу'),
       ('пучок'),
       ('щепотка'),
       ('стебель'),
       ('веточка'),
       ('мл');

insert into category (name)
values ('завтрак'),
       ('обед'),
       ('ужин'),
       ('перекус'),
       ('заготовки'),
       ('выпечка и десерты'),
       ('основные блюда'),
       ('салаты'),
       ('супы'),
       ('паста и пицца'),
       ('сэндвичи'),
       ('напитки'),
       ('соусы'),
       ('бульоны'),
       ('вегетарианская еда'),
       ('веганская еда'),
       ('низкокалорийная еда'),
       ('постная еда'),
       ('быстрые рецепты'),
       ('полезное питание');

ALTER SEQUENCE category_id_seq RESTART WITH 21;

insert into recipe (id, name)
values (1, 'Баклажаны с помидорами и чесноком'),
       (2, 'Баклажаны с растительном масле');

ALTER SEQUENCE recipe_id_seq RESTART WITH 3;

insert into recipe_category (recipe_id, category_id)
values (1, 5),
       (1, 15),
       (2, 2),
       (2, 15);

insert into product (id, name)
values (1, 'баклажан'),
       (2, 'помидор'),
       (3, 'чеснок'),
       (4, 'растительное масло'),
       (5, 'лук репчатый'),
       (6, 'морковь'),
       (7, 'сельдерей(корень)'),
       (8, 'петрушка');

ALTER SEQUENCE product_id_seq RESTART WITH 9;

insert into ingredient (product_id, amount, recipe_id, measure)
VALUES (1, 1, 1, 'кг'),
       (2, 1, 1, 'кг'),
       (3, 600, 1, 'г'),
       (4, 200, 1, 'г'),
       (1, 5, 2, 'кг'),
       (5, 200, 2, 'г'),
       (6, 400, 2, 'г'),
       (7, 200, 2, 'г'),
       (8, 3, 2, 'пучок'),
       (4, 800, 2, 'мл');

ALTER SEQUENCE ingredient_id_seq RESTART WITH 11;

insert into step (recipe_id, description, step_number)
VALUES (1, 'Целые баклажаны очистите и варите 10-15 мин. в подсоленной воде до полуготовности.', 1),
       (1, 'Затем выньте из воды и остудите.', 2),
       (1, 'Остывшие баклажаны нарежьте кружочками, посыпьте перцем и обжарьте до готовности.', 3),
       (1, 'Чеснок растолките, помидоры нарежьте кружочками.', 4),
       (1,
        'Положите на каждый кружочек баклажана чеснок и кружок помидоров, затем сложите в литровые банки, плотно утрамбовывая.',
        5),
       (1, 'Банки залейте оставшимся от жаренья баклажанов маслом.', 6),
       (1,
        'Положите лавровый лист, перец и 0,5 чайной ложки уксуса, накройте крышкой и простерилизуйте 25 мин Затем банки закатайте крышками.',
        7);

insert into step (recipe_id, description, step_number)
VALUES (2,
        'Молодые свежие баклажаны вымойте, отрежьте плодоножки вместе с частью плода и частями бланшируйте в кипящей воде, посоленной из расчета 5 г соли на 1 л воды.',
        1),
       (2,
        'Достав и отцедив воду, баклажаны нарежьте кружками толщиной 2 см, которые затем жарьте в горячем растительном масле 10 мин.',
        2),
       (2, 'Достав из масла, посыпьте баклажаны молотым черным перцем.', 3),
       (2,
        'Поджаренные баклажаны положите в банки емкостью 400 мл, чередуя с кружками лука, моркови, кусочками сельдерея и измельченной петрушки, залейте маслом, в котором они жарились.',
        4),
       (2, 'Банки сразу же закройте герметично и стерилизуйте 15 мин.', 5);

ALTER SEQUENCE step_id_seq RESTART WITH 13;