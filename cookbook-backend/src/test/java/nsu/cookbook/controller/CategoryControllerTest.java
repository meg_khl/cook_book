package nsu.cookbook.controller;

import nsu.cookbook.dto.CategoryDto;
import nsu.cookbook.service.CategoryService;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class CategoryControllerTest {
    private CategoryController categoryController;
    private CategoryService categoryService;
    private List<CategoryDto> categoryDtos = asList(new CategoryDto(1, "1"),
            new CategoryDto(2, "2"));

    @Test
    public void getAllCategories() {
        categoryService = mock(CategoryService.class);
        when(categoryService.getAllCategories()).thenReturn(categoryDtos);
        categoryController = new CategoryController(categoryService);
        List<CategoryDto> controllerCategories = categoryController.getAllCategories();
        List<CategoryDto> serviceCategories = categoryService.getAllCategories();
        assertEquals(serviceCategories.size(), controllerCategories.size());
        for (int i = 0; i < serviceCategories.size(); i++) {
            assertEquals(serviceCategories.get(i).getId(), controllerCategories.get(i).getId());
            assertEquals(serviceCategories.get(i).getName(), controllerCategories.get(i).getName());
        }
    }
}