package nsu.cookbook.controller;

import nsu.cookbook.service.MeasureService;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class MeasureControllerTest {

    private MeasureController measureController;
    private MeasureService measureService;
    private List<String> measures = asList("г", "кг", "л");

    @Test
    public void getMeasures() {
        measureService = mock(MeasureService.class);
        when(measureService.getMeasures()).thenReturn(measures);
        measureController = new MeasureController(measureService);
        assertEquals(measureService.getMeasures(), measureController.getMeasures());
    }
}