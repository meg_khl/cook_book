package nsu.cookbook.controller;

import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.service.ProductService;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ProductControllerTest {

    private ProductController productController;
    private ProductService productService;

    private List<ProductDto> productDtos = asList(new ProductDto(1, "cheese"),
            new ProductDto(2, "milk"));

    @Before
    public void init() {
        productService = mock(ProductService.class);
        when(productService.getAllProducts()).thenReturn(productDtos);
        when(productService.getProductsByNamePart("а")).thenReturn(productDtos);
        productController = new ProductController(productService);
    }

    @Test
    public void getAllProducts() {
        assertEquals(productService.getAllProducts(), productController.getAllProducts());
    }

    @Test
    public void addProduct() {
        ProductDto productDto = new ProductDto();
        ProductService productServiceMock = mock(ProductService.class);
        when(productServiceMock.addProduct(productDto)).thenReturn(productDto);
        ProductController productControllerWithMock = new ProductController(productServiceMock);
        assertEquals(productDto, productControllerWithMock.addProduct(productDto));
        assertEquals(productServiceMock.addProduct(productDto),
                productControllerWithMock.addProduct(productDto));
    }

    @Test
    public void getProductsByNamePart() {
        String namePart = "а";
        assertEquals(productService.getProductsByNamePart(namePart),
                productController.getProductsByNamePart(namePart));
    }
}