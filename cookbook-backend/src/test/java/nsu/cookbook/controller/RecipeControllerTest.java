package nsu.cookbook.controller;

import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.dto.RecipeDto;
import nsu.cookbook.service.RecipeService;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class RecipeControllerTest {
    private RecipeService recipeService = mock(RecipeService.class);
    private RecipeController recipeController = new RecipeController(recipeService);
    private List<RecipeDto> recipeDtos = Arrays.asList(new RecipeDto());
    private List<BaseRecipeDto> baseRecipeDtos = Arrays.asList(new BaseRecipeDto());

    @Test
    public void getAll() {
        when(recipeService.getAll()).thenReturn(baseRecipeDtos);
        assertEquals(recipeService.getAll(), recipeController.getAll());
    }

    @Test
    public void addRecipe() {
        RecipeDto recipeDto = new RecipeDto();
        when(recipeService.addRecipe(recipeDto)).thenReturn(recipeDto);
        assertEquals(recipeService.addRecipe(recipeDto),
                recipeController.addRecipe(recipeDto));
    }

    @Test
    public void getBaseRecipesByCategoriesAndProducts() {
        List<Integer> categoryIds = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> productIds = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        when(recipeService.getBaseRecipesByCategoriesAndProducts(categoryIds, productIds))
                .thenReturn(baseRecipeDtos);
        assertEquals(recipeService.getBaseRecipesByCategoriesAndProducts(categoryIds, productIds),
                recipeController.getBaseRecipesByCategoriesAndProducts(categoryIds, productIds));
    }

    @Test
    public void getRecipeById() {
        int id = 1;
        when(recipeService.getRecipeById(id)).thenReturn(new RecipeDto());
        assertEquals(recipeService.getRecipeById(id),
                recipeController.getRecipeById(id));
    }
}