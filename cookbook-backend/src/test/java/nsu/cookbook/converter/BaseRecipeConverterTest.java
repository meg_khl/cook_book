package nsu.cookbook.converter;

import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.model.Category;
import nsu.cookbook.model.Recipe;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;


public class BaseRecipeConverterTest {
    private final static List<Integer> testIds = asList(1, 2);
    private final static List<String> testNames = asList("recipe1", "recipe2");
    private final static int TEST_DATA_AMOUNT = 2;
    private final static List<Category> testCategories =
            asList(new Category(1, "category1"),
                    new Category(2, "category2"));

    private BaseRecipeConverter baseRecipeConverter =
            new BaseRecipeConverter(new CategoryConverter());

    @Test
    public void convertRecipe() {
        Recipe recipe = createRecipe(0);
        BaseRecipeDto baseRecipeDto = baseRecipeConverter.convertRecipe(recipe);
        assertEquals(recipe.getId(), baseRecipeDto.getId());
        assertEquals(recipe.getName(), baseRecipeDto.getName());
        assertEquals(recipe.getCategories().size(), baseRecipeDto.getCategories().size());
    }

    private Recipe createRecipe(int index) {
        Recipe recipe = new Recipe();
        recipe.setId(testIds.get(index));
        recipe.setName(testNames.get(index));
        recipe.setCategories(asList(testCategories.get(index)));
        return recipe;
    }

    @Test
    public void checkNull() {
        assertEquals(emptyList(), baseRecipeConverter.getBaseRecipeDtos(null));
    }

    @Test
    public void getBaseRecipeDtos() {
        List<Recipe> recipes = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            recipes.add(createRecipe(i));
        }
        List<BaseRecipeDto> baseRecipeDtos = baseRecipeConverter.getBaseRecipeDtos(recipes);
        assertEquals(recipes.size(), baseRecipeDtos.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(recipes.get(i).getId(), baseRecipeDtos.get(i).getId());
            assertEquals(recipes.get(i).getName(), baseRecipeDtos.get(i).getName());
            assertEquals(recipes.get(i).getCategories().size(), baseRecipeDtos.get(i).getCategories().size());
        }
    }
}