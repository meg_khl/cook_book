package nsu.cookbook.converter;

import nsu.cookbook.dto.CategoryDto;
import nsu.cookbook.model.Category;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;


public class CategoryConverterTest {
    private final static int TEST_DATA_AMOUNT = 3;
    private CategoryConverter categoryConverter = new CategoryConverter();
    private List<Integer> testIds = asList(1, 2, 3);
    private List<String> testNames = asList("first", "second", "third");

    @Test
    public void convertCategory() {
        Category category = createCategory(0);
        CategoryDto categoryDto = categoryConverter.convertCategory(category);
        assertEquals(category.getId(), categoryDto.getId());
        assertEquals(category.getName(), categoryDto.getName());
    }

    private Category createCategory(int index) {
        return new Category(testIds.get(index),
                testNames.get(index));
    }

    private CategoryDto createCategoryDto(int index) {
        return new CategoryDto(testIds.get(index),
                testNames.get(index));
    }

    @Test
    public void testForNull() {
        assertEquals(emptyList(), categoryConverter.getCategories(null));
        assertEquals(emptyList(), categoryConverter.getCategoryDtos(null));
    }

    @Test
    public void convertCategoryDto() {
        CategoryDto categoryDto = createCategoryDto(0);
        Category category = categoryConverter.convertCategoryDto(categoryDto);
        assertEquals(categoryDto.getId(), category.getId());
        assertEquals(categoryDto.getName(), category.getName());
    }

    @Test
    public void getCategoryDtos() {
        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            categories.add(createCategory(i));
        }
        List<CategoryDto> categoryDtos = categoryConverter.getCategoryDtos(categories);
        assertEquals(categories.size(), categoryDtos.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(categories.get(i).getId(), categoryDtos.get(i).getId());
            assertEquals(categories.get(i).getName(), categoryDtos.get(i).getName());
        }
    }

    @Test
    public void getCategories() {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            categoryDtos.add(createCategoryDto(i));
        }
        List<Category> categories = categoryConverter.getCategories(categoryDtos);
        assertEquals(categoryDtos.size(), categories.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(categoryDtos.get(i).getId(), categories.get(i).getId());
            assertEquals(categoryDtos.get(i).getName(), categories.get(i).getName());
        }
    }
}