package nsu.cookbook.converter;

import nsu.cookbook.dto.IngredientDto;
import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.model.Ingredient;
import nsu.cookbook.model.Measure;
import nsu.cookbook.model.Product;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;


public class IngredientConverterTest {
    private int id = 1, productId = 1;
    private int amount = 2;
    private String measure = "г", productName = "product";
    private ProductDto productDto = new ProductDto(productId, productName);

    private IngredientConverter ingredientConverter =
            new IngredientConverter(new ProductConverter(), new MeasureConverter());

    @Test
    public void getIngredient() {
        IngredientDto ingredientDto = new IngredientDto(id, amount, measure, productDto);
        Ingredient ingredient = ingredientConverter.getIngredient(ingredientDto);
        checkEquals(ingredientDto, ingredient);
    }

    private void checkEquals(IngredientDto ingredientDto, Ingredient ingredient) {
        assertEquals(ingredientDto.getId(), ingredient.getId());
        assertEquals(ingredientDto.getAmount(), ingredient.getAmount());
        assertEquals(ingredientDto.getMeasure(), ingredient.getMeasure().getName());
        assertEquals(ingredientDto.getProductDto().getId(),
                ingredient.getProduct().getId());
        assertEquals(ingredientDto.getProductDto().getName(),
                ingredient.getProduct().getName());
    }

    @Test
    public void getIngredientDto() {
        Ingredient ingredient = new Ingredient(id,
                amount,
                new Product(productId, productName),
                new Measure(measure));
        IngredientDto ingredientDto = ingredientConverter.getIngredientDto(ingredient);
        checkEquals(ingredientDto, ingredient);
    }

    private void checkEquals(Ingredient ingredient, IngredientDto ingredientDto) {
        assertEquals(ingredient.getId(), ingredientDto.getId());
        assertEquals(ingredient.getAmount(), ingredientDto.getAmount());
        assertEquals(ingredient.getMeasure().getName(), ingredientDto.getMeasure());
        assertEquals(ingredient.getProduct().getId(),
                ingredientDto.getProductDto().getId());
        assertEquals(ingredient.getProduct().getName(),
                ingredientDto.getProductDto().getName());
    }

    @Test
    public void testForNull() {
        assertEquals(emptyList(), ingredientConverter.getIngredientDtos(null));
        assertEquals(emptyList(), ingredientConverter.getIngredients(null));
    }

    @Test
    public void getIngredients() {
        List<IngredientDto> ingredientDtos = asList(new IngredientDto(id, amount, measure, productDto),
                new IngredientDto(id + 1, amount + 1, measure, productDto));
        List<Ingredient> ingredients = ingredientConverter.getIngredients(ingredientDtos);
        assertEquals(ingredientDtos.size(), ingredients.size());
        for (int i = 0; i < ingredientDtos.size(); i++) {
            checkEquals(ingredientDtos.get(i), ingredients.get(i));
        }
    }

    @Test
    public void getIngredientDtos() {
        List<Ingredient> ingredients = asList(
                new Ingredient(id, amount, new Product(productId, productName), new Measure(measure)),
                new Ingredient(id - 1, amount - 1, new Product(productId, productName), new Measure(measure)));
        List<IngredientDto> ingredientDtos = ingredientConverter.getIngredientDtos(ingredients);
        assertEquals(ingredients.size(), ingredientDtos.size());
        for (int i = 0; i < ingredientDtos.size(); i++) {
            checkEquals(ingredients.get(i), ingredientDtos.get(i));
        }
    }
}