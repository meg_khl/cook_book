package nsu.cookbook.converter;

import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.model.Product;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;

public class ProductConverterTest {
    private final static int TEST_DATA_AMOUNT = 3;
    private ProductConverter productConverter = new ProductConverter();
    private int testId = 10;
    private String testName = "testProduct";
    private List<Integer> testIds = asList(1, 2, 3);
    private List<String> testNames = asList("first", "second", "third");

    @Test
    public void getProduct() {
        Product convertedProduct = productConverter
                .getProduct(new ProductDto(testId, testName));
        assertEquals(testId, convertedProduct.getId());
        assertEquals(testName, convertedProduct.getName());
    }

    @Test
    public void getProductDto() {
        ProductDto convertedProductDto = productConverter
                .getProductDto(new Product(testId, testName));
        assertEquals(testId, convertedProductDto.getId());
        assertEquals(testName, convertedProductDto.getName());
    }

    @Test
    public void getProductDtosWithNullList() {
        assertEquals(emptyList(),
                productConverter.getProductDtos(null));
    }

    @Test
    public void getProductDtos() {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            products
                    .add(new Product(testIds.get(i),
                            testNames.get(i)));
        }
        List<ProductDto> productDtos = productConverter.getProductDtos(products);
        assertEquals(products.size(), productDtos.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(products.get(i).getId(),
                    productDtos.get(i).getId());
            assertEquals(products.get(i).getName(),
                    productDtos.get(i).getName());
        }
    }
}