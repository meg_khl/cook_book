package nsu.cookbook.converter;

import nsu.cookbook.dto.StepDto;
import nsu.cookbook.model.Step;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;


public class StepConverterTest {
    private final static int TEST_INDEX = 0;
    private final static int TEST_DATA_AMOUNT = 4;
    private List<Integer> testIds = asList(1, 2, 3, 4);
    private List<String> testDescriptions = asList("first",
            "second", "third", "fourth");
    private List<Integer> testStepNumbers = asList(5, 6, 7, 8);
    private StepConverter stepConverter = new StepConverter();

    @Test
    public void getStep() {
        Step step = stepConverter
                .getStep(new StepDto(testIds.get(TEST_INDEX),
                        testStepNumbers.get(TEST_INDEX),
                        testDescriptions.get(TEST_INDEX)));
        assertEquals((int) testIds.get(TEST_INDEX), step.getId());
        assertEquals((int) testStepNumbers.get(TEST_INDEX), step.getStepNumber());
        assertEquals(testDescriptions.get(TEST_INDEX), step.getDescription());
    }

    @Test
    public void getStepDto() {
        StepDto stepDto = stepConverter
                .getStepDto(new Step(testIds.get(TEST_INDEX),
                        testDescriptions.get(TEST_INDEX),
                        testStepNumbers.get(TEST_INDEX)));
        assertEquals((int) testIds.get(TEST_INDEX), stepDto.getId());
        assertEquals((int) testStepNumbers.get(TEST_INDEX), stepDto.getStepNumber());
        assertEquals(testDescriptions.get(TEST_INDEX), stepDto.getDescription());
    }

    @Test
    public void testForNull() {
        assertEquals(emptyList(), stepConverter.getStepDtos(null));
        assertEquals(emptyList(), stepConverter.getSteps(null));
    }

    @Test
    public void getStepDtos() {
        List<Step> steps = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            steps.add(new Step(testIds.get(i),
                    testDescriptions.get(i),
                    testStepNumbers.get(i)));
        }
        List<StepDto> stepDtos = stepConverter.getStepDtos(steps);
        assertEquals(steps.size(), stepDtos.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(steps.get(i).getId(), stepDtos.get(i).getId());
            assertEquals(steps.get(i).getDescription(), stepDtos.get(i).getDescription());
            assertEquals(steps.get(i).getStepNumber(), stepDtos.get(i).getStepNumber());
        }
    }

    @Test
    public void getSteps() {
        List<StepDto> stepDtos = new ArrayList<>();
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            stepDtos.add(new StepDto(testIds.get(i),
                    testStepNumbers.get(i),
                    testDescriptions.get(i)));
        }
        List<Step> steps = stepConverter.getSteps(stepDtos);
        assertEquals(steps.size(), stepDtos.size());
        for (int i = 0; i < TEST_DATA_AMOUNT; i++) {
            assertEquals(stepDtos.get(i).getId(), steps.get(i).getId());
            assertEquals(stepDtos.get(i).getDescription(), steps.get(i).getDescription());
            assertEquals(stepDtos.get(i).getStepNumber(), steps.get(i).getStepNumber());
        }
    }
}