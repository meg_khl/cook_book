package nsu.cookbook.dto;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@SpringBootTest
public class BaseRecipeDtoTest {
    private int id = 1;
    private String name = "name";
    private List<CategoryDto> categories = asList(new CategoryDto());
    private BaseRecipeDto baseRecipeDtoForTests = new BaseRecipeDto();

    @Test
    public void allArgsConstructorTest() {
        BaseRecipeDto baseRecipeDto = new BaseRecipeDto(id, name, categories);
        assertEquals(id, baseRecipeDto.getId());
        assertEquals(name, baseRecipeDto.getName());
        assertEquals(categories, baseRecipeDto.getCategories());
    }

    @Test
    public void getId() {
        baseRecipeDtoForTests.setId(id);
        assertEquals(id, baseRecipeDtoForTests.getId());
    }

    @Test
    public void getName() {
        baseRecipeDtoForTests.setName(name);
        assertEquals(name, baseRecipeDtoForTests.getName());
    }

    @Test
    public void getCategories() {
        baseRecipeDtoForTests.setCategories(categories);
        assertEquals(categories, baseRecipeDtoForTests.getCategories());
    }
}