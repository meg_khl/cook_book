package nsu.cookbook.dto;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class IngredientDtoTest {
    private int testId = 1;
    private int testAmount = 10;
    private String testMeasure = "testMeasure";
    private int testProductId = 2;
    private String testName = "testName";
    private ProductDto productDto = new ProductDto(testProductId, testName);


    @Test
    public void testAllArgsConstructor() {
        IngredientDto ingredientDto = new IngredientDto(testId, testAmount, testMeasure, productDto);
        assertEquals(testId, ingredientDto.getId());
        assertEquals(testAmount, ingredientDto.getAmount());
        assertEquals(testMeasure, ingredientDto.getMeasure());
        assertEquals(productDto, ingredientDto.getProductDto());
    }

    @Test
    public void testSet() {
        IngredientDto ingredientDto = new IngredientDto();
        ingredientDto.setId(testId);
        assertEquals(testId, ingredientDto.getId());
        ingredientDto.setAmount(testAmount);
        assertEquals(testAmount, ingredientDto.getAmount());
        ingredientDto.setMeasure(testMeasure);
        assertEquals(testMeasure, ingredientDto.getMeasure());
        ingredientDto.setProductDto(productDto);
        assertEquals(productDto, ingredientDto.getProductDto());
    }
}