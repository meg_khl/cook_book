package nsu.cookbook.dto;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class ProductDtoTest {
    private int id = 1;
    private String name = "name";
    private ProductDto productDto = new ProductDto();

    @Test
    public void testEquals() {
        assertTrue(productDto.equals(productDto));
        assertFalse(productDto.equals(null));
        assertFalse(productDto.equals(new IngredientDto()));
        ProductDto product = new ProductDto(id, name);
        assertTrue(product.equals(new ProductDto(id, name)));
        assertFalse(product.equals(new ProductDto(id + 1, name)));
        assertFalse(product.equals(new ProductDto(id, name + " ")));
    }

    @Test
    public void testHashCode() {
        ProductDto productDto = new ProductDto(id, name);
        assertTrue(productDto.hashCode() == Objects.hash(productDto.getId(), productDto.getName()));
    }

    @Test
    public void setId() {
        productDto.setId(id);
        assertEquals(id, productDto.getId());
    }

    @Test
    public void setName() {
        productDto.setName(name);
        assertEquals(name, productDto.getName());
    }
}