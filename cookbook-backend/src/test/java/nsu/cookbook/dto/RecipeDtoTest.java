package nsu.cookbook.dto;

import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class RecipeDtoTest {
    private int id = 1;
    private String name = "test";
    private List<CategoryDto> categories = asList(new CategoryDto());
    private List<IngredientDto> ingredients = asList(new IngredientDto());
    private List<StepDto> steps = asList(new StepDto());
    private RecipeDto recipeDto = new RecipeDto();

    @Test
    public void setIngredients() {
        recipeDto.setIngredients(ingredients);
        assertEquals(ingredients, recipeDto.getIngredients());
    }

    @Test
    public void setSteps() {
        recipeDto.setSteps(steps);
        assertEquals(steps, recipeDto.getSteps());
    }

    @Test
    public void testAllArgsConstructor() {
        RecipeDto recipeDto = new RecipeDto(id, name, categories, ingredients, steps);
        assertEquals(id, recipeDto.getId());
        assertEquals(name, recipeDto.getName());
        assertEquals(categories, recipeDto.getCategories());
        assertEquals(ingredients, recipeDto.getIngredients());
        assertEquals(steps, recipeDto.getSteps());
    }
}