package nsu.cookbook.dto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StepDtoTest {
    private int id = 1;
    private int stepNumber = 2;
    private String description = "test";
    private StepDto stepDto = new StepDto();

    @Test
    public void setId() {
        stepDto.setId(id);
        assertEquals(id, stepDto.getId());
    }

    @Test
    public void setStepNumber() {
        stepDto.setStepNumber(stepNumber);
        assertEquals(stepNumber, stepDto.getStepNumber());
    }

    @Test
    public void setDescription() {
        stepDto.setDescription(description);
        assertEquals(description, stepDto.getDescription());
    }
}