package nsu.cookbook.model;

import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryTest {

    @Test
    public void testConstructor() {
        int id = 2;
        String name = "выпечка";
        Category category = new Category(id, name);
        assertEquals(id, category.getId());
        assertEquals(name, category.getName());
    }

    @Test
    public void testSet() {
        List<Recipe> recipes = asList(new Recipe());
        Category category = new Category();
        category.setRecipes(recipes);
        assertEquals(recipes, category.getRecipes());
        int id = 1;
        String name = "test";
        category.setId(id);
        assertEquals(id, category.getId());
        category.setName(name);
        assertEquals(name, category.getName());
    }

}