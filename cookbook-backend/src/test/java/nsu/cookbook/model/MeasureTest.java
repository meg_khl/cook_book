package nsu.cookbook.model;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class MeasureTest {
    private String measureTestName = "test_measure";

    @Test
    public void testAllArgsConstructor() {
        Measure constructedMeasure = new Measure(measureTestName);
        assertEquals(measureTestName, constructedMeasure.getName());
    }
}