package nsu.cookbook.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProductTest {
    private int testId = 1;
    private String testName = "test_product";

    @Test
    public void setId() {
        Product product = new Product();
        product.setId(testId);
        assertEquals(testId, product.getId());
    }

    @Test
    public void setName() {
        Product product = new Product();
        product.setName(testName);
        assertEquals(testName, product.getName());
    }
}