package nsu.cookbook.service;

import nsu.cookbook.dao.IngredientDao;
import nsu.cookbook.model.Ingredient;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IngredientServiceTest {
    @Test
    public void addIngredients() {
        ProductService productService = mock(ProductService.class);
        MeasureService measureService = mock(MeasureService.class);
        IngredientDao ingredientDao = mock(IngredientDao.class);
        List<Ingredient> allIngredients = new ArrayList<>();
        List<Ingredient> ingredients = asList(new Ingredient());
        when(ingredientDao.saveAll(ingredients)).then(invocation -> {
            allIngredients.addAll(ingredients);
            return null;
        }).thenReturn(ingredients);
        when(ingredientDao.findAll()).thenReturn(allIngredients);
        IngredientService ingredientServiceWithMock = new IngredientService(ingredientDao,
                productService, measureService);
        ingredientServiceWithMock.addIngredients(ingredients);
        assertTrue(allIngredients.containsAll(ingredients));
    }
}