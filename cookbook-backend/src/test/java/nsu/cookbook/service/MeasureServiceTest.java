package nsu.cookbook.service;

import nsu.cookbook.dao.MeasureDao;
import nsu.cookbook.model.Measure;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MeasureServiceTest {
    private MeasureService measureService;
    private MeasureDao measureDao;
    private List<Measure> measures = asList(new Measure("г"),
            new Measure("кг"));
    private List<String> measureString = asList("г", "кг");

    @Before
    public void init() {
        measureDao = mock(MeasureDao.class);
        when(measureDao.findAll()).thenReturn(measures);
        measureService = new MeasureService(measureDao);
    }

    @Test
    public void getMeasures() {
        List<String> measureNames = measureService.getMeasures();
        List<Measure> measures = measureDao.findAll();
        assertEquals(measures.size(), measureNames.size());
        for (int i = 0; i < measures.size(); i++) {
            assertEquals(measures.get(i).getName(), measureNames.get(i));
        }
    }

    @Test
    public void testForNull() {
        MeasureDao measureDao = mock(MeasureDao.class);
        when(measureDao.findAll()).thenReturn(null);
        MeasureService measureServiceForNull = new MeasureService(measureDao);
        assertEquals(emptyList(), measureServiceForNull.getMeasures());
    }

    @Test
    public void addMeasure() {
        MeasureDao measureDao = mock(MeasureDao.class);
        String testName = "testName";
        Measure measure = new Measure(testName);
        List<Measure> measures = asList(measure);
        when(measureDao.save(measure)).thenReturn(measure);
        when(measureDao.findAll()).thenReturn(measures);
        MeasureService measureServiceForAdd = new MeasureService(measureDao);
        measureServiceForAdd.addMeasure(measure);
        assertTrue(measureServiceForAdd.getMeasures().contains(measure.getName()));
    }
}