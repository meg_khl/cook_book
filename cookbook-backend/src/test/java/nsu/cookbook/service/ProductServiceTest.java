package nsu.cookbook.service;

import nsu.cookbook.converter.ProductConverter;
import nsu.cookbook.dao.ProductDao;
import nsu.cookbook.dto.ProductDto;
import nsu.cookbook.model.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ProductServiceTest {
    private int STARTED_PRODUCTS_AMOUNT = 2;
    private ProductService productService;
    private List<ProductDto> productDtos = asList(new ProductDto(1, "cheese"),
            new ProductDto(2, "milk"));

    @Before
    public void init() {
        productService = mock(ProductService.class);
        when(productService.getAllProducts()).thenReturn(productDtos);
    }

    @Test
    public void addProduct() {
        ProductDao productDao = mock(ProductDao.class);
        ProductConverter productConverter = mock(ProductConverter.class);
        List<Product> allProducts = new ArrayList<>();
        Product product = new Product();
        when(productDao.save(product)).then(invocation -> {
            allProducts.add(product);
            return null;
        }).thenReturn(product);
        ProductService productService = new ProductService(productDao, productConverter);
        productService.addProduct(product);
        assertTrue(allProducts.size() > 0);
        assertTrue(allProducts.contains(product));
    }

    @Test
    public void getAllProducts() {
        List<ProductDto> products = productService.getAllProducts();
        assertTrue(products.size() >= STARTED_PRODUCTS_AMOUNT);
    }
}