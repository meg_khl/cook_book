package nsu.cookbook.service;

import nsu.cookbook.converter.*;
import nsu.cookbook.dao.RecipeDao;
import nsu.cookbook.dto.BaseRecipeDto;
import nsu.cookbook.model.Recipe;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class RecipeServiceTest {
    private RecipeDao recipeDao = mock(RecipeDao.class);

    private StepService stepService = mock(StepService.class);
    private IngredientService ingredientService = mock(IngredientService.class);

    private BaseRecipeConverter baseRecipeConverter = new BaseRecipeConverter(new CategoryConverter());
    private RecipeConverter recipeConverter = new RecipeConverter(new IngredientConverter(new ProductConverter(), new MeasureConverter()),
            new CategoryConverter(), new StepConverter());

    private RecipeService recipeService = new RecipeService(recipeDao,
            baseRecipeConverter, recipeConverter, stepService, ingredientService);

    private Recipe recipe;

    @Before
    public void createRecipe() {
        recipe = new Recipe();
        recipe.setId(1);
        recipe.setName("name");
        recipe.setCategories(Collections.emptyList());
    }

    @Test
    public void getAll() {
        List<Recipe> recipies = asList(recipe);
        when(recipeDao.findAll()).thenReturn(recipies);
        List<BaseRecipeDto> expectedDtos = baseRecipeConverter.getBaseRecipeDtos(recipies);
        List<BaseRecipeDto> actualDtos = recipeService.getAll();
        assertEquals(expectedDtos.size(), actualDtos.size());
        for (int i = 0; i < expectedDtos.size(); i++) {
            assertEquals(expectedDtos.get(i).getId(), actualDtos.get(i).getId());
            assertEquals(expectedDtos.get(i).getName(), actualDtos.get(i).getName());
            assertEquals(expectedDtos.get(i).getCategories().size(),
                    actualDtos.get(i).getCategories().size());
        }
    }
}