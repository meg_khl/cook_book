package nsu.cookbook.service;

import nsu.cookbook.dao.StepDao;
import nsu.cookbook.model.Step;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StepServiceTest {

    private StepService stepService = mock(StepService.class);

    @Test
    public void addSteps() {
        stepService.addSteps(null);
        List<Step> allSteps = new ArrayList<>();
        List<Step> steps = asList(new Step());
        StepDao stepDao = mock(StepDao.class);
        when(stepDao.saveAll(steps)).then(invocation -> {
            allSteps.addAll(steps);
            return null;
        }).thenReturn(steps);
        StepService stepMockService = new StepService(stepDao);
        stepMockService.addSteps(steps);
        assertTrue(allSteps.size() > 0);
        assertTrue(allSteps.containsAll(steps));
    }
}