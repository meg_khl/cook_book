import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import recipes from "@/components/Recipes/Recipes"
import addRecipeOrProduct from "@/components/AddRecipeOrProduct/AddRecipeOrProduct"
import recipe from "@/components/Recipes/Recipe"
export default new Router({
  routes: [
    {
      path: '/',
      name: 'recipes',
      component: recipes
    },
    {
      path: '/add-recipe-or-product',
      name: 'addRecipeOrProduct',
      component: addRecipeOrProduct
    },
    {
      path: '/recipe/:id',
      component: recipe,
      props: true
    }
  ]
})
