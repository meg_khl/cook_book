import Axios from "axios";

export default class RestService {

  static getRecipes() {
    return Axios.get('http://localhost:10000/recipes/get-all');
  }

  static getCategories() {
    return Axios.get('http://localhost:10000/categories/get-all');
  }

  static getProducts() {
    return Axios.get('http://localhost:10000/products/get-all');
  }

  static getMeasures() {
    return Axios.get('http://localhost:10000/measure/get-all');
  }

  static addProduct(dto) {
    return Axios({
      method: 'post',
      url: 'http://localhost:10000/products/add',
      data: dto
    });
  }

  static addRecipe(dto) {
    return Axios({
      method: 'post',
      url: 'http://localhost:10000/recipes/add-recipe',
      data: dto
    });
  }

  static getRecipeById(id) {
    return Axios({
      method: 'get',
      url: 'http://localhost:10000/recipes/get-by-id',
      params: {id}
    });
  }

  static getRecipesByCategoriesAndProducts(dto) {
    return Axios({
      method: 'get',
      url: 'http://localhost:10000/recipes/get-base-by-categories-and-products',
      params: {
        categoryIds: dto.categoryIds.join(','),
        productIds: dto.productIds.join(',')
      }
    });
  }

}
